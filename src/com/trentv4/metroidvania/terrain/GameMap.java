package com.trentv4.metroidvania.terrain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import com.trentv4.metroidvania.entity.Entity;
import com.trentv4.pliable.LogLevel;
import com.trentv4.pliable.Logger;
import com.trentv4.pliable.MainGame;

public class GameMap
{
    public ArrayList<Entity> entityList = new ArrayList<Entity>();
    public ArrayList<Terrain> terrainList = new ArrayList<Terrain>();

    private GameMap()
    {
    }

    public static final GameMap getMap(String path)
    {
        GameMap map = new GameMap();
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(new File("maps/" + path)));
            int f = 0;
            String q = reader.readLine();
            while (q != null)
            {
                f++;
                try
                {
                    if (q.charAt(0) != '#')
                    {
                        String[] a = q.split(",");
                        Terrain.getTerrain(Terrain.parse(a[4]).terrainId, map).setPos(Integer.parseInt(a[0]), Integer.parseInt(a[1]))
                                .setSize(Integer.parseInt(a[2]), Integer.parseInt(a[3]));
                    }

                } catch (Exception e)
                {
                    Logger.log(LogLevel.ERROR, "Unable to load line " + f + "!");
                }
                q = reader.readLine();
            }
            reader.close();
        } catch (Exception e)
        {
            MainGame.crash(new RuntimeException("Unable to load map: " + path + " !"));
        }
        return map;
    }
}
