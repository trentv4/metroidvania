package com.trentv4.metroidvania.terrain;

import static com.trentv4.metroidvania.GameStructureMetroidvania.WORLD_SCALE;
import static com.trentv4.metroidvania.GameStructureMetroidvania.camera;

import java.util.ArrayList;

import com.trentv4.metroidvania.GameStructureMetroidvania;
import com.trentv4.pliable.LogLevel;
import com.trentv4.pliable.Logger;
import com.trentv4.pliable.MainGame;
import com.trentv4.pliable.Renderer;

public class Terrain
{
    public int x;
    public int y;
    public int vecX;
    public int vecY;

    public int xSize;
    public int ySize;
    public String texture;
    public boolean isSolid;

    public int terrainId;
    public int UUID;

    private static int _maxTerrainId = 0;
    private static int _maxUUID = 0;

    private static ArrayList<Terrain> terrainTypeList = new ArrayList<Terrain>();

    public static final Terrain ter_box = new Terrain().setTexture("terrain/wall1.png").setSolid(true);
    public static final Terrain back_star = new Terrain().setTexture("backgrounds/stars.png").setSolid(false);
    public static final Terrain back_metal = new Terrain().setTexture("backgrounds/metal.png").setSolid(false);

    private Terrain()
    {
        this.isSolid = true;
        this.texture = "";
        this.terrainId = _maxTerrainId;
        _maxTerrainId++;
        terrainTypeList.add(this);
    }

    public Terrain setTexture(String texture)
    {
        this.texture = texture;
        return this;
    }

    public Terrain setSolid(boolean isSolid)
    {
        this.isSolid = isSolid;
        return this;
    }

    public Terrain setPos(int x, int y)
    {
        this.x = x * WORLD_SCALE;
        this.y = y * WORLD_SCALE;
        return this;
    }

    public Terrain setSize(int x, int y)
    {
        this.xSize = x * WORLD_SCALE;
        this.ySize = y * WORLD_SCALE;
        return this;
    }

    public Terrain setVectors(int x, int y)
    {
        this.vecX = x;
        this.vecY = y;
        return this;
    }

    public Terrain addVectors(int x, int y)
    {
        this.vecX += x;
        this.vecY += y;
        return this;
    }

    public static final Terrain getTerrain(Terrain entity)
    {
        return getTerrain(entity.terrainId);
    }

    public static final Terrain getTerrain(int id)
    {
        Terrain t = new Terrain();
        if (!(id >= terrainTypeList.size()))
        {
            Terrain base = terrainTypeList.get(id);

            // Assign ids
            t.terrainId = base.terrainId;
            t.UUID = _maxUUID;
            _maxUUID++;

            // Copy traits
            t.texture = base.texture;
            t.isSolid = base.isSolid;

            GameStructureMetroidvania.map.terrainList.add(t);
        } else
        {
            Logger.log(LogLevel.ERROR, "Unable to get terrain: " + id);
        }
        return t;
    }

    public static final Terrain parse(String s)
    {
        for (int i = 0; i < terrainTypeList.size(); i++)
        {
            if (terrainTypeList.get(i).texture.equals(s))
            {
                return terrainTypeList.get(i);
            }
        }
        MainGame.crash(new RuntimeException("Unable to parse terrain: " + s + " !"));
        return null;
    }

    public static final Terrain getTerrain(int id, GameMap map)
    {
        Terrain t = new Terrain();
        if (!(id >= terrainTypeList.size()))
        {
            Terrain base = terrainTypeList.get(id);

            // Assign ids
            t.terrainId = base.terrainId;
            t.UUID = _maxUUID;
            _maxUUID++;

            // Copy traits
            t.texture = base.texture;
            t.isSolid = base.isSolid;

            map.terrainList.add(t);
        } else
        {
            Logger.log(LogLevel.ERROR, "Unable to get terrain: " + id);
        }
        return t;
    }

    public void tick()
    {
        this.x += vecX;
        this.y += vecY;
    }

    public void render(Renderer renderer)
    {
        for (int i = 0; i < xSize; i += WORLD_SCALE)
        {
            for (int z = 0; z < ySize; z += WORLD_SCALE)
            {
                renderer.drawImage(texture, (i + x + camera[0]), (z + y + camera[1]), GameStructureMetroidvania.WORLD_SCALE,
                        GameStructureMetroidvania.WORLD_SCALE);
            }
        }
    }
}
