package com.trentv4.metroidvania;

import com.trentv4.metroidvania.entity.Entity;
import com.trentv4.metroidvania.terrain.Terrain;

public abstract class CoreUtil
{
    public static final boolean checkTerrainCollision(int x, int y)
    {
        Terrain[] a = GameStructureMetroidvania.map.terrainList.toArray(new Terrain[GameStructureMetroidvania.map.terrainList.size()]);
        for (Terrain t : a)
        {
            if (x > t.x)
            {
                if (x < t.x + t.xSize)
                {
                    if (y > t.y)
                    {
                        if (y < t.y + t.ySize)
                        {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static final boolean checkTerrainCollision(Entity e, int vecX, int vecY)
    {
        int x = e.x + vecX;
        int y = e.y + vecY;
        Terrain[] a = GameStructureMetroidvania.map.terrainList.toArray(new Terrain[GameStructureMetroidvania.map.terrainList.size()]);
        for (Terrain t : a)
        {
            if (t.isSolid)
            {
                if (x + e.xSize > t.x)
                {
                    if (x < t.x + t.xSize)
                    {
                        if (y + e.ySize > t.y)
                        {
                            if (y < t.y + t.ySize)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}
