package com.trentv4.metroidvania.entity;

import static com.trentv4.metroidvania.GameStructureMetroidvania.player;

public class AIProgramBasic implements AIProgram
{
    @Override
    public void invoke(Entity e)
    {
        if (e.x < player.x)
            e.addVectors(2, 0);
        if (e.x > player.x)
            e.addVectors(-2, 0);
        if (e.y < player.y)
            e.addVectors(0, 2);
        if (e.y > player.y)
            e.addVectors(0, -2);
        if (Math.abs((e.x - player.x) + (e.y - player.y)) < 5)
        {
            // player.dealDamage(2);
        }
    }
}
