package com.trentv4.metroidvania.entity;

public interface AIProgram
{
    public void invoke(Entity e);
}
