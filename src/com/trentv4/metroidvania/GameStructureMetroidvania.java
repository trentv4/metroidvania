package com.trentv4.metroidvania;

import java.awt.Color;

import com.trentv4.metroidvania.entity.Entity;
import com.trentv4.metroidvania.terrain.GameMap;
import com.trentv4.metroidvania.terrain.Terrain;
import com.trentv4.pliable.GameStructure;
import com.trentv4.pliable.MainGame;
import com.trentv4.pliable.Renderer;

public class GameStructureMetroidvania extends GameStructure
{
    public static Entity player;
    public static GameMap map;

    public static int[] camera = new int[] { 50, 50 };

    public static int[][] position = new int[][] { { 0, 0 }, { 0, 0 } };

    public static final int WORLD_SCALE = 25;
    // public static final int WORLD_SCALE = 8;
    public static final int GRAVITY_CONST = 3;

    @Override
    public void initialize()
    {
        map = GameMap.getMap("default.mp");
        player = Entity.getEntity(Entity.player).setPos(-2000, -2000);
    }

    public static void eval()
    {
        System.out.println(position[0][0] + "," + position[0][1] + "," + Math.abs(position[0][0] - position[1][0]) + ","
                + Math.abs(position[0][1] - position[1][1]));
    }

    @Override
    public void draw(Renderer renderer)
    {
        Terrain[] terrain = map.terrainList.toArray(new Terrain[0]);
        for (int i = 0; i < terrain.length; i++)
        {
            terrain[i].render(renderer);
        }
        Entity[] entity = map.entityList.toArray(new Entity[0]);
        for (int i = 0; i < entity.length; i++)
        {
            entity[i].render(renderer);
        }
        renderer.drawRectangle(Color.red, position[0][0], position[0][1], Math.abs(position[0][0] - position[1][0]),
                Math.abs(position[0][1] - position[1][1]));
        renderer.drawText(MainGame.version, Color.red, 0, 0, 2);
    }

    @Override
    public void tick()
    {
        Entity[] entity = map.entityList.toArray(new Entity[0]);
        for (int i = 0; i < entity.length; i++)
        {
            entity[i].tick();
        }
        // camera = new int[]{-player.x - (player.xSize/2) +
        // (DisplayManager.width/2), -player.y - (player.ySize/2) +
        // (DisplayManager.height/2) + 50};
    }

    public static void kill()
    {

    }
}
