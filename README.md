# Metroidvania #

### How do I get set up? ###

* Open the project in your preferred IDE. The libraries used (LWJGL) are distributed with it. Natives are shipped alongside it.

### Contribution guidelines ###

* All contributions must be well documented, well-written, and easy to understand and modify.

### Who do I talk to? ###

* Trent VanSlyke. Found here: trentvanslyke@gmail.com, @trentv4, twitter.com/trentv4, jamieswhiteshirt.com/trentv4